#!/bin/bash

# Just make sure you setup sudo before you set any of the other things up
# Remember to check the packages to ensure they meet your needs

install='apt-get install -y'
# Basic compiler toolchain
compiler_toolchain="gdb build-essential gcc make binutils"
# Music related stuff
musics='mocp'
# Things for writing
writing='pandoc texlive zathura'
# Other amenities things whicih are really just misc things
rev="wireshark curl zsh"

web_things() {
	if [ -z $1 ] 
	then
		$install tor
		wget 'http://ftp.gnu.org/gnu/gnuzilla/60.3.0/icecat-60.3.0.en-US.gnulinux-x86_64.tar.bz2'
		echo 'Attempting to setup icecat now!'
		tar -xf icecat-60.3.0.en-US.gnulinux-x86_64.tar.bz2
		ln -s "`pwd`/icecat/icecat" /usr/bin/icecat
	else
		$install $rev
	fi
}
 


dotfiles() {
	# Personal dotfiles 
	cp -n dotfiles/.zshrc $HOME/.zshrc
}

reverse_bin() {
	# Radare2
	mkdir -p $HOME/Programs/
	git clone https://github.com/radare/radare2 $HOME/Programs/radare2
	$HOME/Programs/radare2/sys/install.sh
}

basics() {
	$install $compiler_toolchain
	# Because u need a browser dummy
	web_things
	$install $musics $writing $rev
}

help() {
	echo 'Options: 
	d dotfiles
	w web things
	r reversing tools(binary)'
}

if [ -z "$1" ];then
	help
	exit 0
fi

while getopts ':hdwr' OPTION; do
	case "$OPTION" in 
		h) help;;
		d) dotfiles;;
		w) web_things ;;
		r) reverse_bin;;
		*) 
			echo './min.sh -h for help'
			exit 0
			;;
	esac
done
