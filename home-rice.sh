#!/bin/sh

# Some of these require bash so if this fails it's the distro's fault

if [ -d "$HOME/Rice/" ];then
	cd $HOME/Rice/
	for f in bread bright newbg proj snip ytdl;do
		sudo ./new $f
	done
else
	echo No $HOME/Rice found!
fi
