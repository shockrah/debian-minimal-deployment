#!/bin/sh

# Dotfiles
cp required-rice/vimrc ~/.vimrc
cp required-rice/zshrc ~/.zshrc

# Rust comes last because of the interactive prompt
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

