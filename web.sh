#!/bin/sh

# Basic things to install web based tools and programs

install="apt install --no-install-recommends -y"

$install tor apt-transport-https curl wget

# Brave installation straight from their own page
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
apt update
apt install brave-browser

# Tor installation
printf "deb http://deb.debian.org/debian buster-backports main contrib" > /etc/apt/sources.list.d/buster-backports.list
apt update
apt install torbrowser -t buster-backports


# Discord
echo NOT INSTALLING DISCORD BUT HERE IS THE COMMAND TO DO GET THE .DEB FILE
echo 'wget https://discordapp.com/api/download?platform=linux&format=deb'
