#!/bin/sh

# Script builds all the things I personally need for my regular desktop experience
install='apt install -y'

$install cmake gcc build-essential binutils zsh meson \
	pandoc texlive zathura vim-nox snapd p7zip-full

$install pulseaudio pavucontrol moc

$install imagemagick inkscape gimp mpv

$install git-lfs wget

# This is mostly for polybar and other building thigns
$install cmake cmake-data libcairo2-dev libxcb1-dev \
	libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev \
	libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev \
	pkg-config python-xcbgen xcb-proto libxcb-xrm-dev \
	i3-wm libasound2-dev libmpdclient-dev libiw-dev \
	libcurl4-openssl-dev libpulse-dev libxcb-composite0-dev


sh ./x-stuff.sh
sh ./web.sh
