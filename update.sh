#!/bin/bash

# Updates dotfiles based on system dot files
cp $HOME/.vimrc ./
cp $HOME/.zshrc ./
