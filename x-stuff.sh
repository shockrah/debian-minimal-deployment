#!/bin/sh

# valid options are : intel nouveau ati
vid='nouveau'

if [ -z "$vid" ]
then
	echo 'SET THE  $vid VARIABLE TO YOUR NEEDS BEFORE PROCEEDING'
	exit
fi
# This is used to build things like dwm later on
install="apt install --no-install-recommends -y"


# Xorg core
$install xserver-xorg-core \
	xserver-xorg-input-all \
	xserver-xorg-video-fbdev \
	xserver-xorg-video-$vid \
	xorg xserver-xorg

# For background images and general ricing
$install feh compton

# For polybar later on
$install cmake cmake-data libcairo2-dev libxcb1-dev \
	libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev \
	libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev \
	pkg-config python-xcbgen xcb-proto libxcb-xrm-dev \
	i3-wm libasound2-dev libmpdclient-dev libiw-dev \
	libcurl4-openssl-dev libpulse-dev libxcb-composite0-dev


# Required x-utilities
$install dbus fonts-dejavu-core xinit

# multiple screens on a single display support + font drawing in X
$install libxinerama1 libxft2

# For building DWM later
$install git ca-certificates \
	build-essential \
	libx11-dev libxinerama-dev libxft-dev

echo 'All done, here are some git links to cool sofwares =|:^)'
echo 'https://git.suckless.org/dmenu'
echo 'https://git.suckless.org/dwm'
echo 'https://gitlab.com/shockrahwow/st-fork'
