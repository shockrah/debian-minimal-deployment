#!/bin/sh
# Add user to sudo  group cuz i can never remember that command
# Must be ran as root (duh)

if [ -z $1 ];then
	echo No username provided to add
	exit 1
fi

usermod -aG sudo "$1"
