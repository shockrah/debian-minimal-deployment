" ycm autocomplete thingy
set completeopt-=preview
" Load pathogen 
execute pathogen#infect()

" Basic things to make life ez
set background=dark
set autoindent
set shiftwidth=0
set tabstop=4
set relativenumber
set number
set numberwidth=5
set hlsearch
set clipboard=unnamedplus
syntax on
colorscheme molokai

" Better defautl behavior for splits 
set splitbelow
set splitright

" moving around split windows (how it should be)
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Dealing with latex
autocmd FileType tex setlocal makeprg=pdflatex\ --shell-escape\ '%'
autocmd Filetype tex setlocal spell
autocmd Filetype tex setlocal linebreak 
" Dealing with markdown
autocmd FileType md setlocal makeprg=pandoc\ '%'
