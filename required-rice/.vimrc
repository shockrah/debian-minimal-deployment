" Pathogen stuff from here
execute pathogen#infect()

" Generally good settings
syntax on
filetype plugin indent on
set backspace=indent,eol,start
set background=dark
set autoindent
set shiftwidth=0
set tabstop=4
set relativenumber
set number
set numberwidth=5
set hlsearch
set ignorecase
set cinoptions+=l1
set colorcolumn=80
let g:ftplugin_sql_omni_key = '<C-j>'


" colors 
syntax on
let g:limelight_conceal_ctermfg = 240
"colorscheme molokai

" vim default for this is dumb
set splitright 
set splitbelow

" moving around windows
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" YAML is really busted so here are some fixes in config form
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" Writing stuff
let g:unite_redpen_default_config_path = "/home/shockrah/.vim/bundle/unite-redpen.vim/redpen-conf.xml"
let g:unite_redpen_command = "redpen"

autocmd Filetype markdown set spell
autocmd Filetype markdown :set linebreak
" activate some plugins to make writing instantly better
autocmd Filetype markdown :Goyo
"autocmd Filetype markdown :Limelight
"autocmd Filetype markdown setlocal makeprg=pandoc\ % -o %:r.md

" Pandoc compiling 
" Tex compiling
autocmd Filetype tex setlocal makeprg=pdflatex\ %
autocmd Filetype md inoremap <F5> :!echo<space>pandoc<space>-fmarkdown-implicit_figures<space>-f<space>markdown<space>-t<space>pdf<space>%<enter>
autocmd Filetype tex inoremap ,mp :!pdflatex %
