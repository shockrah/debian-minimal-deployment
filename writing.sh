# Here we're just going to download and setup some things which i personally
# use for ricey purposes

# TODO: replace this with the source version of vim to get python/language support
install="apt install --no-install-recommends -y"
# for mac users with brew
#install="brew install"

$install vim

# Import dotfiles now
cp vimrc $HOME/.vimrc

# Colors
mkdir -p $HOME/.vim/colors/
cp molokai.vim $HOME/.vim/colors/molokai.vim

# I do lots of writing so these plugins are nice for just writing
mkdir -p "$HOME/.vim/autoload" "$HOME/.vim/bundle" && \
	curl -LSso "$HOME/.vim/autoload/pathogen.vim" "https://tpo.pe/pathogen.vim"

# For writing documents
git clone https://github.com/junegunn/goyo.vim "$HOME/.vim/bundle/goyo"
git clone https://github.com/junegunn/limelight.vim "$HOME/.vim/bundle/limelight"

# For writing code
git clone https://github.com/Vimjas/vim-python-pep8-indent "$HOME/.vim/bundle/vim-python-pep8-indent"
git clone --recursive https://github.com/davidhalter/jedi-vim.git ~/.vim/bundle/jedi-vim

# Finally install the only font you'll ever need
mkdir -p "$HOME/.fonts"
cp FSEX300.ttf "$HOME/.fonts/"
fc-cache -f -v
